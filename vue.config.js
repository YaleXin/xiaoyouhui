/*
 * @Author      : YaleXin
 * @Email       : me@yalexin.top
 * @LastEditors : YaleXin
 */
const path = require('path');
function resolve(dir) {
    return path.join(__dirname, dir)
}
module.exports = {
    devServer: {
        port: 8081,
        proxy: {
            '/school_home_serv': {
                target: 'http://localhost:8080/school_home_serv/',
                ws: false,
                changeOrigin: true,
                pathRewrite: {
                    '^/school_home_serv': '',
                }
            },
            '/jinri': {
                target: 'http://v1.jinrishici.com/',
                ws: false,
                changeOrigin: true,
                pathRewrite: {
                    '^/jinri': '',
                }
            },
        },
        disableHostCheck: true,
    },
    // assetsDir: 'static',
    // parallel: false,
    // publicPath: process.env.NODE_ENV === 'production' ? '/blog/' : '/',
    outputDir: 'blog',
    assetsDir: 'static',
    configureWebpack: (config) => {
        // config.optimization.minimizer[0].options.terserOptions.compress.drop_console =true;
    }
}