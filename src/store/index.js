/*
 * @Author      : YaleXin
 * @Email       : 181303209@yzu.edu.cn
 * @LastEditors : YaleXin
 */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      username: -1,
      nickname: '',
      password: ''
    },
    adminUser: {
      username: ''
    }
  },
  mutations: {
    saveUser(state, user) {
      state.user = user;
    },
    removeUser(state) {
      state.user = {
        username: -1,
        nickname: '',
        password: ''
      }
    },
    saveAdminUser(state) {
      state.adminUser.username = "admin"
    },
    removeAdminUser(state) {
      state.adminUser = {
        username: ''
      }
    },
  },
  actions: {
  },
  modules: {
  }
})
