/*
 * @Author      : YaleXin
 * @Email       : 181303209@yzu.edu.cn
 * @LastEditors : YaleXin
 */

import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
const Home = () => import("@/views/Home")
const Talk = () => import("@/views/Talk")
const Donation = () => import("@/views/Donation")
const DonationIndex = () => import("@/components/DonationIndex")
const DonationHistory = () => import("@/components/DonationHistory")
const DonationInput = () => import("@/components/DonationInput")
const Cooperation = () => import("@/views/Cooperation")
const CooperationIndex = () => import("@/components/CooperationIndex")
const CooperationInput = () => import("@/components/CooperationInput")
const Software = () => import("@/views/Software")
const Admin = () => import("@/views/Admin")
const AdminIndex = () => import("@/views/admin/AdminIndex")
const AdminTalk = () => import("@/views/admin/AdminTalk")
const AdminCooperation = () => import("@/views/admin/AdminCooperation")
const AdminSoftware = () => import("@/views/admin/AdminSoftware")
const AdminDonation = () => import("@/views/admin/AdminDonation")
const AdminLogin = () => import("@/views/admin/AdminLogin")
const Login = () => import("@/views/Login")

const adminRoute = [
  {
    path: '',
    redirect: 'index',
  },
  {
    path: 'index',
    component: AdminIndex
  },
  {
    path: 'talk',
    component: AdminTalk
  },
  {
    path: 'cooperation',
    component: AdminCooperation
  },
  {
    path: 'donation',
    component: AdminDonation
  },
  {
    path: 'software',
    component: AdminSoftware
  },
  {
    path: 'login',
    component: AdminLogin
  },
]

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    component: Home
  },
  {
    path: '/talk',
    component: Talk
  },
  {
    path: '/donation',
    component: Donation,
    children: [
      {
        path: '/',
        redirect: 'index'
      },
      {
        path: 'index',
        component: DonationIndex
      },
      {
        path: 'history',
        component: DonationHistory
      },
      {
        path: 'input',
        component: DonationInput
      },
    ]
  },
  {
    path: '/cooperation',
    component: Cooperation,
    children: [
      {
        path: '/',
        redirect: 'index'
      },
      {
        path: 'index',
        component: CooperationIndex
      },
      {
        path: 'input',
        component: CooperationInput
      }

    ]
  },
  {
    path: '/software',
    component: Software
  },
  {
    path: '/admin',
    component: Admin,
    children: adminRoute
  },
  {
    path: '/login',
    component: Login,
  }
]

const router = new VueRouter({
  mode: 'history',
  // mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
